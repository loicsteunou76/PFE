import tensorflow as tf
import numpy as np
import os
import scipy.io
from reseaux import SportCNN

#PARAMETRES
#Données
#[1 0 0] = EXP
#[0 1 0] = BEG
#[0 0 1] = FIR
tf.flags.DEFINE_integer("EXP_SIZE", 3359, "Données sur des experts")
tf.flags.DEFINE_integer("BEG_SIZE", 3938, "Données sur des débutants")
tf.flags.DEFINE_integer("FIR_SIZE", 4786, "Données sur des firemans")
tf.flags.DEFINE_integer("DATA_SIZE", 12083, "Nombre de données")
tf.flags.DEFINE_integer("nb_releve", 2000, "Nombre de relevé de données")
tf.flags.DEFINE_integer("nb_data", 9, "Nombre de données par relevé")

#Fichier de sortie
tf.flags.DEFINE_string("out_dir", "out_dir", "Dossier de sortie")
tf.flags.DEFINE_string("Fichier_train", "out_file_train", "Fichier")
tf.flags.DEFINE_string("Fichier_test", "out_file_test", "Fichier")
tf.flags.DEFINE_string("Fichier_parametre", "out_file_param", "Fichier")

#Taille de la base de test
tf.flags.DEFINE_float("testing_data_percentage", 0.1, "Proportion de données utilisee dans la base de test")

#Paramètres du réseau
tf.flags.DEFINE_integer("nb_filters", 30, "Nombre de filtre par taille")
tf.flags.DEFINE_float("prob_dropout", 1.0, "Probabilité de garder un neurones")
tf.flags.DEFINE_integer("hauteur_filtres", 50, "Hauteur filtre de conv")
tf.flags.DEFINE_integer("largeur_filtres", 9, "Largeur filtre de conv")

#Paramètres d'apprentissage
tf.flags.DEFINE_integer("batch_size", 100, "Batch Size")
tf.flags.DEFINE_integer("epochs", 450, "Number of training epochs")
tf.flags.DEFINE_integer("test_eval", 100, "Evaluation sur la base de test toutes les X étapes")
tf.flags.DEFINE_integer("train_eval", 10, "Evaluation sur la base d'APPRENTISSAGE toutes les X étapes")

FLAGS = tf.flags.FLAGS
FLAGS._parse_flags()
print("\nParameters:")
for attr, value in sorted(FLAGS.__flags.items()):
    print("{}={}".format(attr.upper(), value))
print("")

adressFichierParametre = FLAGS.out_dir+"/"+FLAGS.Fichier_parametre+".txt"
fichier_param = open(adressFichierParametre, "w")
for attr, value in sorted(FLAGS.__flags.items()):
    fichier_param.write(attr.upper()+"="+str(value))
    fichier_param.write("\n")

#CHARGEMENT DES DONNEES
#Données
DATA = scipy.io.loadmat('test_data_esca.mat')
DATA_SET = DATA['DATA_SET']
LABEL_SET = DATA['LABEL_SET']
data64 = DATA_SET[:,0]
data32=np.array([np.float32(i) for i in data64])

#Labels
def vectorized_digit(j): #Transforme les labels (1,2,3) en vecteur [1,0,0],[0,1,0],[0,0,1]
    e = np.zeros(3)
    e[j-1] = 1.0
    return e
labels = LABEL_SET[:,0]
vectorized_labels = np.array([np.float32(vectorized_digit(i)) for i in labels])
print("Donnees chargees !")

#CREATION/MELANGE DES BASES
np.random.seed(10)
shuffle_indices = np.random.permutation(np.arange(len(data32)))

#Mélange des données et des labels selon les mêmes indices
data_shuffled = data32[shuffle_indices]
labels_shuffled = vectorized_labels[shuffle_indices]

#Découpage de ces données en deux BASES
cut_data_ind = int(FLAGS.testing_data_percentage * float(len(labels)))
data_test, data_train = data_shuffled[:cut_data_ind], data_shuffled[cut_data_ind:]
labels_test, labels_train = labels_shuffled[:cut_data_ind], labels_shuffled[cut_data_ind:]

#Obtention des batchs
def get_batch(train_data, train_labels, batch_size, ind):
        batch_x = np.array([j for j in train_data[ind:ind+batch_size]])
        batch_y = np.array([j for j in train_labels[ind:ind+batch_size]])
        return batch_x, batch_y

#Step d'apprentissage
def train_step(batch_x, batch_y):
    #Propagation et apprentissage sur le batch = (batch_x, batch_y)
    feed_dict = {cnn.input_x: batch_x, cnn.y_: batch_y, cnn.keep_prob: FLAGS.prob_dropout}
    _, step, predictions, loss, precision, input_argmax = sess.run([train_op, global_step, cnn.predictions,cnn.loss, cnn.precision,cnn.input_argmax],feed_dict)
    #Précision par classe
    expR = 0
    expL = 0
    begR = 0
    begL = 0
    firR = 0
    firL = 0
    for i, val in enumerate(predictions):
        if input_argmax[i]==0:
            if val==0:
                expR+=1
            else:
                expL+=1
        if input_argmax[i]==1:
            if val==1:
                begR+=1
            else:
                begL+=1
        if input_argmax[i]==2:
            if val==2:
                firR+=1
            else:
                firL+=1
    pourE = expR/(expR+expL)
    pourB = begR/(begR+begL)
    pourF = firR/(firR+firL)
    if (step%10==0):
        print("step {}, loss {:g}, acc {:g}, Pourcentage exp reussi {:g}, pourcentage beg reussi {:g}, pourcentage fir reussi {:g}, nb exp : {:g}, nb beg : {:g}, nb fir : {:g} ".format(step, loss, precision,pourE,pourB,pourF,expR+expL,begR+begL,firR+firL))
        print(" ")
    if(step%100==0):
        ecrit = "{:g} ; {:g} ; {:g} ; {:g} ; {:g}".format(step,round(precision,2),round(pourE,2),round(pourB,2),round(pourF,2))
        fichier_train.write(ecrit)
        fichier_train.write("\n")

#Step de test
def dev_step(batch_x, batch_y):
    #Propagation du batch = (batch_x, batch_y)
    feed_dict = {cnn.input_x: batch_x, cnn.y_: batch_y, cnn.keep_prob: 1.0}
    step, predictions, loss, precision, input_argmax= sess.run([global_step, cnn.predictions, cnn.loss, cnn.precision, cnn.input_argmax], feed_dict)
    #Précision par classe
    expR = 0
    expL = 0
    begR = 0
    begL = 0
    firR = 0
    firL = 0
    for i, val in enumerate(predictions):
        if input_argmax[i]==0:
            if val==0:
                expR+=1
            else:
                expL+=1
        if input_argmax[i]==1:
            if val==1:
                begR+=1
            else:
                begL+=1
        if input_argmax[i]==2:
            if val==2:
                firR+=1
            else:
                firL+=1
    pourE = expR/(expR+expL)
    pourB = begR/(begR+begL)
    pourF = firR/(firR+firL)
    print("step {}, loss {:g}, acc {:g}, Pourcentage exp reussi {:g}, pourcentage beg reussi {:g}, pourcentage fir reussi {:g}, nb exp : {:g}, nb beg : {:g}, nb fir : {:g}".format(step, loss, precision,pourE,pourB,pourF,expR+expL,begR+begL,firR+firL))
    print(" ")
    ecrit = "{:g} ; {:g} ; {:g} ; {:g} ; {:g}".format(step,round(precision,2),round(pourE,2),round(pourB,2),round(pourF,2))
    fichier_test.write(ecrit)
    fichier_test.write("\n")

sess = tf.Session()
cnn = SportCNN(FLAGS.nb_filters)

#MODELE APPRENTISSAGE
global_step = tf.Variable(0, name="global_step", trainable=False)

#Optimisation de l'apprentissage
optimizer = tf.train.AdamOptimizer(1e-4)
grads_and_vars = optimizer.compute_gradients(cnn.loss)
train_op = optimizer.apply_gradients(grads_and_vars, global_step=global_step)

#Initialisation
sess.run(tf.global_variables_initializer())

#Gestion des fichiers de sortie
addressFichierTrain = FLAGS.out_dir+"/"+FLAGS.Fichier_train+".txt"
fichier_train = open(addressFichierTrain, "w")
ecrit = ("Step:     acc :    Exp Réussi :     Beg Réussi :     Fir Réussi :")
ecrit = str(ecrit)
fichier_train.write(ecrit)
fichier_train.write("\n")

adressFichierTest = FLAGS.out_dir+"/"+FLAGS.Fichier_test+".txt"
fichier_test = open(adressFichierTest, "w")
ecrit = ("Step:     acc :    Exp Réussi :     Beg Réussi :     Fir Réussi :")
ecrit = str(ecrit)
fichier_test.write(ecrit)
fichier_test.write("\n")


#BOUCLE ENTRAINEMENT
nb_batch = len(data_train)/FLAGS.batch_size
print("Il y aura",nb_batch-1," batch et ",FLAGS.epochs," epochs.")
for i in range(FLAGS.epochs):
    print("Epochs n° {:g}".format(i))
    shuffle_indices = np.random.permutation(np.arange(len(data_train)))
    x_train = data_train[shuffle_indices]
    y_train = labels_train[shuffle_indices]
    ind = 0
    while (ind<len(x_train)-FLAGS.batch_size):
        batch_x, batch_y = get_batch(x_train,y_train,FLAGS.batch_size,ind)
        res = train_step(batch_x,batch_y)
        current_step = tf.train.global_step(sess, global_step)
        if current_step % 100 == 0 :
            print("Evaluation")
            dev_step(data_test, labels_test)
        ind+=FLAGS.batch_size
print("Entrainement Terminer")
print("Last test :")
dev_step(data_test, labels_test)
