import tensorflow as tf
import numpy as np

class SportCNN(object):
    def __init__(self, nb_filters):
        #Définition des placeholders
        with tf.name_scope("inputs"):
            #Entrées x (Pour nos données)
            self.input_x = tf.placeholder(tf.float32, shape=[None, 2000, 9], name="input_x")
            #Reshape des entrées x
            self.x3d = tf.reshape(self.input_x, [-1, 2000, 9, 1], name = "x3d")
            #Labels de nos données
            self.y_ = tf.placeholder(tf.float32, [None, 3], name = "y")

        #Gestion du dropout
        self.keep_prob=tf.placeholder(tf.float32, name="keep_prob")

        #CONVOLUTION
        with tf.name_scope("convolution"):
            #Taille des filtres de convolution
            filtre_shape = [50, 9, 1, nb_filters]

            #Variable des poids de la convolution
            W_conv1 = tf.Variable(tf.truncated_normal(filtre_shape, stddev=0.1), name="W_conv1")

            #Variable des biais de la convolution
            b_conv1 = tf.Variable(tf.constant(0.1, shape = [nb_filters]), name="b_conv1")

            #Application de la convolution avec la taille de décalage et le padding
            conv1 = tf.nn.conv2d(self.x3d, W_conv1, [1,10,1,1], padding='VALID', name="conv1")

            #Application de la fonction sigmoïde
            h_conv1 = tf.nn.sigmoid(tf.nn.bias_add(conv1, b_conv1), name="h_conv1")

            #Reshape de la sortie de la couche de convolution
            h_conv1_flat = tf.reshape(h_conv1,[-1,196*1*nb_filters], name="h_conv1_flat")

        #Couches entièrements connectées
        with tf.name_scope("fullyconnected"):
            #Couche 1
            #Variable des poids (5880 -> 1500 neurones)
            W_fc1 = tf.Variable(tf.truncated_normal([196*1*nb_filters,1500], stddev=0.1), name="W_fc1")

            #Variable des biais
            b_fc1 = tf.Variable(tf.constant(0.1, shape = [1500]), name="b_fc1")

            #Calcul du produit matricielle et application de la fonction sigmoïde
            h_fc1 = tf.nn.sigmoid(tf.matmul(h_conv1_flat, W_fc1)+b_fc1, name ="h_fc1")

            #Application du dropout
            h_fc1_drop = tf.nn.dropout(h_fc1,self.keep_prob, name ="h_fc1_drop")

            #Couche 2
            #Variable des poids (1500 -> 600 neurones)
            W_fc2 = tf.Variable(tf.truncated_normal([1500,600], stddev=0.1), name="W_fc2")

            #Variable des biais
            b_fc2 = tf.Variable(tf.constant(0.1, shape = [600]), name="b_fc2")

            #Calcul du produit matricielle et application de la fonction sigmoïde
            h_fc2 = tf.nn.sigmoid(tf.matmul(h_fc1_drop, W_fc2) + b_fc2, name="h_fc2")

            #Application du dropout
            h_fc2_drop = tf.nn.dropout(h_fc2,self.keep_prob, name ="h_fc2_drop")

        #Couche de sortie
        with tf.name_scope("output"):
            #Variable des poids (600 -> 3)
            W_fc3 = tf.Variable(tf.truncated_normal([600, 3], stddev=0.1), name="W_fc3")

            #Variable des biais
            b_fc3 = tf.Variable(tf.constant(0.1, shape = [3]), name="b_fc3")

            #Calcul de la sortie
            self.output = tf.nn.sigmoid(tf.matmul(h_fc2_drop, W_fc3)+b_fc3, name="output")

            #Prédiction du réseau
            self.predictions = tf.argmax(self.output, 1, name="predictions")

        #Losses
        with tf.name_scope("losses"):
            #Calcul de l'erreur
            losses = tf.nn.softmax_cross_entropy_with_logits(logits = self.output, labels = self.y_, name="loss")
            self.loss = tf.reduce_mean(losses, name="reduce-mean-loss")

        #Precision
        self.input_argmax = tf.argmax(self.y_, 1)
        with tf.name_scope("precision"):
            correct_predictions = tf.equal(self.predictions,self.input_argmax)
            self.precision = tf.reduce_mean(tf.cast(correct_predictions, "float"), name="precision")
